import java.sql.Array;

public class LigneFractale {

    private final Point depart;
    private final Point arrivee;


    LigneFractale[] segments = new LigneFractale[] {null, null, null, null, null};

    public LigneFractale(Point depart, Point arrivee, int ordre) {

        //System.out.println("Nouvelle Ligne fractale d'ordre " + ordre);

        //verifierDistanceMini(depart, arrivee);
        this.depart = depart;
        this.arrivee = arrivee;

        if (ordre > 0) {
            //calculerSegments(ordre);
            calculerLigne(ordre);
        }
        else {
            this.segments = null;
        }


    }

    private void calculerLigne(int ordre) {

        int angle = calculerAngle(this.getDepart(), this.getArrivee());
        double longueurSegment = Math.sqrt(
                Math.pow(this.getArrivee().getX() - this.getDepart().getX(), 2)
                        + Math.pow(this.getArrivee().getY() - this.getDepart().getY(), 2)
        );
        int pas = Math.floorDiv((int) longueurSegment, 4);


    }

    private void calculerSegments(int ordre) {
        int k = Math.floorDiv(this.getArrivee().getX() - this.getDepart().getX(), 4);

        Point ini1 = new Point(this.getDepart().getX(), this.getDepart().getY());
        Point ini2 = new Point(this.getDepart().getX() + k, this.getDepart().getY());
        Point ini3 = new Point(this.getDepart().getX() + k, this.getDepart().getY() + k);
        Point ini4 = new Point(this.getDepart().getX() + 2 * k, this.getDepart().getY() + k);
        Point ini5 = new Point(this.getDepart().getX() + 2 * k, this.getDepart().getY());
        Point ini6 = new Point(this.getArrivee().getX(), this.getArrivee().getY());

        this.segments[0] = pivoterLigneFractale(new LigneFractale(ini1, ini2, ordre - 1), 0);
        this.segments[1] = pivoterLigneFractale(new LigneFractale(ini2, ini3, ordre - 1), 270);
        this.segments[2] = pivoterLigneFractale(new LigneFractale(ini3, ini4, ordre - 1), 90);
        this.segments[3] = new LigneFractale(ini4, ini5, ordre - 1);
        this.segments[4] = new LigneFractale(ini5, ini6, ordre - 1);

    }

    private LigneFractale pivoterLigneFractale(LigneFractale fract, int angleDegres) {
        //
    }

    private void verifierDistanceMini(Point depart, Point arrivee) {
        if ( Math.abs( arrivee.getX() - depart.getX()) < 4) {
            throw new IllegalArgumentException("La distance entre les absisses de deux points doit etre au minimum de 4.");
        }
    }

    public LigneFractale[] getSegments() {
        return segments;
    }


    public Point getArrivee() {
        return arrivee;
    }

    public Point getDepart() {
        return depart;
    }
}
