public class ServiceFractale {

    public static LigneFractale creerLigneFractale(int x1, int y1, int x2, int y2, int ordre) {
        return new LigneFractale(new Point(x1,y1), new Point(x2, y2), ordre);
    }

}