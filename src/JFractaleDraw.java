import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class JFractaleDraw extends JComponent {

    private static class Line {
        final int x1;
        final int y1;
        final int x2;
        final int y2;
        final Color color;

        public Line(int x1, int y1, int x2, int y2, Color color) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.color = color;
        }
    }

    private final LinkedList<Line> lines = new LinkedList<Line>();

    public void addLine(int x1, int y1, int x2, int y2) {
        addLine(x1, y1, x2, y2, Color.black);
    }

    public JFractaleDraw(LigneFractale lf) {

        LinkedList<Point[]> llp = getAllPoints(lf);
        for (Point[] cp : llp ) {
                addLine(cp[0].x,cp[0].y,cp[1].x,cp[1].y);
        }

    }

    private LinkedList<Point[]> getAllPoints(LigneFractale lf) {
        LinkedList<Point[]> result = new LinkedList<Point[]>();

        if ( lf.getSegments() == null) {
            result.add(
                    new Point[]{
                            new Point(lf.getDepart().getX(), lf.getDepart().getY()),
                            new Point(lf.getArrivee().getX(), lf.getArrivee().getY())
                    }
                    );
        }
        else {
            for (LigneFractale lf1 : lf.getSegments()) {
                result.addAll(getAllPoints(lf1));
            }
        }
        return result;
    }

    public void addLine(int x1, int y1, int x2, int y2, Color color) {
        lines.add(new Line(x1, y1, x2, y2, color));
        repaint();
    }

    public void clearLines() {
        lines.clear();
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Line line : lines) {
            g.setColor(line.color);
            g.drawLine(line.x1, line.y1, line.x2, line.y2);
        }
    }
}