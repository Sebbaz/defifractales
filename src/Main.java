import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {

        //jc.setBackground(Color.WHITE);
        //jc.setPreferredSize(new Dimension(400,200));
        //GUIHelper.showOnFrame(jc,"test");

        // Init graph
        JFrame testFrame = new JFrame();
        testFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


        // Job
        LigneFractale lf = ServiceFractale.creerLigneFractale(0,0,1000,0,2);
        final JFractaleDraw jf = new JFractaleDraw(lf);
        jf.setPreferredSize(new Dimension(320, 200));
        testFrame.getContentPane().add(jf, BorderLayout.CENTER);
        testFrame.pack();
        testFrame.setVisible(true);

    }
}